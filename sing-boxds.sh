#!/bin/bash

set -euo pipefail

VERSION="1.0.0"

RED='\033[0;31m'
NC='\033[0m' # No Color

check_system_requirements() {
  echo -e "${RED}$(date): 开始检查系统要求${NC}"
  [ "$(uname -m)" != "x86_64" ] && echo -e "${RED}Sing-Box 仅支持 64 位系统。${NC}" && exit 1

  source /etc/os-release
  if [[ "$ID" == "ubuntu" ]]; then
    if (( $(echo "$VERSION_ID < 18.04" | bc -l) )); then
      echo -e "${RED}Sing-Box 要求 Ubuntu 18.04 LTS 或更高版本。${NC}" && exit 1
    fi
  elif [[ "$ID" == "debian" ]]; then
    if (( $(echo "$VERSION_ID < 12" | bc -l) )); then
      echo -e "${RED}Sing-Box 要求 Debian 12 或更高版本。${NC}" && exit 1
    fi
  else
    echo -e "${RED}Sing-Box 仅支持 Ubuntu 和 Debian 系统。${NC}" && exit 1
  fi
  echo -e "${RED}$(date): 系统要求检查完成${NC}"
}

update_software_repositories() {
  echo -e "${RED}$(date): 开始更新软件仓库${NC}"
  apt update || true
  echo -e "${RED}$(date): 软件仓库更新完成${NC}"
}

install_required_software() {
  echo -e "${RED}$(date): 开始安装必要的软件${NC}"
  apt install -y sudo unzip wget curl vim net-tools bc
  echo -e "${RED}$(date): 必要的软件安装完成${NC}"
}

download_sing_box() {
  echo -e "${RED}$(date): 开始下载 Sing-Box${NC}"
  local install_dir="$1"
  local package_url="https://gitcode.net/qq_43662828/metaclash/-/raw/master/sing-box_1.7.4_linux_amd64.deb"
  local package_path="${install_dir}/sing-box_1.7.4_linux_amd64.deb"

  wget "${package_url}" -P "${install_dir}"
  [ ! -f "${package_path}" ] && echo -e "${RED}下载的 Sing-Box 安装包不存在。${NC}" && exit 1
  echo -e "${RED}$(date): Sing-Box 下载完成${NC}"
}

install_sing_box() {
  echo -e "${RED}$(date): 开始安装 Sing-Box${NC}"
  dpkg -i "$1"
  echo -e "${RED}$(date): Sing-Box 安装完成${NC}"
}

start_sing_box_service() {
  echo -e "${RED}$(date): 开始启动 Sing-Box 服务${NC}"
  systemctl start sing-box
  echo -e "${RED}$(date): Sing-Box 服务启动完成${NC}"
}

download_config_file() {
  echo -e "${RED}$(date): 开始下载配置文件${NC}"
  local config_url="https://gitcode.net/qq_43662828/metaclash/-/raw/master/sing-box/sing-box.zip"
  local config_path="/etc/sing-box"

  wget "${config_url}" -P "${config_path}"
  [ ! -f "${config_path}/sing-box.zip" ] && echo -e "${RED}下载的配置文件不存在。${NC}" && exit 1
  echo -e "${RED}$(date): 配置文件下载完成${NC}"
}

extract_config_file() {
  echo -e "${RED}$(date): 开始解压配置文件${NC}"
  local config_path="/etc/sing-box"

  unzip -o "${config_path}/sing-box.zip" -d "${config_path}"
  echo -e "${RED}$(date): 配置文件解压完成${NC}"
}

print_message() {
  echo -e "${RED}$(date): Sing-Box 安装成功。${NC}"
  echo "请使用以下命令启动 Sing-Box："
  echo "systemctl start sing-box"
}

print_server_info() {
  echo -e "${RED}$(date): 服务器信息${NC}"
  echo "后台访问地址: $(hostname -I | awk '{print $1}'):9090/ui"
  echo "后台访问密码: 1381ni"
  echo "安装路径: /opt/sing-box"
  echo "配置文件路径: /etc/sing-box/config.json"
  echo "操作方法: 使用 systemctl start sing-box 启动服务"
  echo "操作方法: 使用 systemctl status sing-box 查看服务"
}

main() {
  echo "sing-box x86 scu install"
  check_system_requirements
  update_software_repositories
  install_required_software
  download_sing_box "/opt/sing-box"
  install_sing_box "/opt/sing-box/sing-box_1.7.4_linux_amd64.deb"
  start_sing_box_service
  download_config_file
  extract_config_file
  print_message
  print_server_info
  echo "👏欢迎大佬加入群组TG👏"
  echo "群组TG@zhetengbiji"
  echo "https://t.me/zhetengbiji"
}

main