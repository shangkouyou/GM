# 把配置文件里面tun 的部分，替换为下面tproxy： 
        {
        "type": "tproxy",
        "tag": "tproxy-in",
        "listen": "::",
        "listen_port": 3344,
        "tcp_fast_open": true,
        "udp_fragment": true,
        "sniff": true,
        "sniff_override_destination": true,
        "sniff_timeout": "300ms",
        "udp_timeout": 300
    },

# 在auto interface 下面加上 default mark 这句:
        "auto_detect_interface": true,
        "default_mark": 1,
    },

#创建
sudo touch /etc/systemd/system/singbox-route.service
sudo nano /etc/systemd/system/singbox-route.service
=======================================================================================================================================================================================================
[Unit]
Description=sing-box TProxy Rules
After=network.target
Wants=network.target

[Service]
User=root
Type=oneshot
RemainAfterExit=yes
# there must be spaces before and after semicolons
ExecStart=/sbin/ip rule add fwmark 1 table 100 ; /sbin/ip route add local default dev lo table 100 ; /sbin/ip -6 rule add fwmark 1 table 101 ; /sbin/ip -6 route add local ::/0 dev lo table 101
ExecStop=/sbin/ip rule del fwmark 1 table 100 ; /sbin/ip route del local default dev lo table 100 ; /sbin/ip -6 rule del fwmark 1 table 101 ; /sbin/ip -6 route del local ::/0 dev lo table 101

[Install]
WantedBy=multi-user.target
========================================================================================================================================================================================================
systemctl enable --now singbox-route
systemctl restart singbox-route

systemctl enable --now singbox-route && systemctl restart singbox-route


#nftables配置
nano /etc/nftables.conf
=========================================================================================================================================================================================================
#!/usr/sbin/nft -f

flush ruleset

define RESERVED_IP = {
    100.64.0.0/10,
    127.0.0.0/8,
    169.254.0.0/16,
    172.16.0.0/12,
    192.0.0.0/24,
    224.0.0.0/4,
    240.0.0.0/4,
    255.255.255.255/32
}

table ip sing-box {
        chain prerouting {
                type filter hook prerouting priority mangle; policy accept;
                ip daddr $RESERVED_IP return
                # 修改为你的内网网段
                ip daddr 10.0.0.0/24 tcp dport != 53 return
                ip daddr 10.0.0.0/24 udp dport != 53 return
                # 修改为你的透明代理程序的端口
                ip protocol tcp tproxy to :3344 meta mark set 1
                ip protocol udp tproxy to :3344 meta mark set 1
        }
        chain output {
                type route hook output priority mangle; policy accept;
                ip daddr $RESERVED_IP return
                # 修改为你的内网网段
                ip daddr 10.0.0.0/24 tcp dport != 53 return
                ip daddr 10.0.0.0/24 udp dport != 53 return
                meta mark set 1234 return
                ip protocol tcp meta mark set 1
                ip protocol udp meta mark set 1
        }
}
======================================================================================================================================================================================================
nft flush ruleset
nft -f /etc/nftables.conf
nft list ruleset
systemctl enable --now nftables
systemctl restart nftables

nft flush ruleset && nft -f /etc/nftables.conf && nft list ruleset && systemctl enable --now nftables && systemctl restart nftables

#自启动
nano /etc/systemd/system/nftables1.service
======================================================================================================================================================================================================
[Unit]
Description=NFTables Firewall
After=network.target

[Service]
ExecStart=/usr/sbin/nft -f /etc/nftables.conf
Type=oneshot
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
======================================================================================================================================================================================================
systemctl enable --now nftables1
systemctl restart nftables1

systemctl enable --now nftables1 && systemctl restart nftables1 && systemctl enable --now sing-box