#!/bin/bash

# 函数：下载并解压文件
download_and_unzip() {
    local url="$1"
    local target_dir="$2"

    echo -e "\033[31m$(date): 开始下载并解压 $url 到 $target_dir\033[0m"
    if wget "$url" -O temp.zip && unzip temp.zip -d "$target_dir" && rm -f temp.zip; then
        echo -e "\033[31m$(date): 下载并解压完成\033[0m"
    else
        echo -e "\033[31m$(date): 下载并解压 $url 失败\033[0m"
        exit 1
    fi
}

# 函数：创建目录
create_directory() {
    local directory="$1"

    echo -e "\033[31m$(date): 开始创建目录 $directory\033[0m"
    if mkdir -p "$directory"; then
        echo -e "\033[31m$(date): 创建目录完成\033[0m"
    else
        echo -e "\033[31m$(date): 创建目录 $directory 失败\033[0m"
        exit 1
    fi
}

# 函数：创建文件
create_file() {
    local file="$1"
    local content="$2"

    echo -e "\033[31m$(date): 开始创建文件 $file\033[0m"
    if echo "$content" > "$file"; then
        echo -e "\033[31m$(date): 创建文件完成\033[0m"
    else
        echo -e "\033[31m$(date): 创建文件 $file 失败\033[0m"
        exit 1
    fi
}

# 函数：安装并启动 mosdns 服务
install_and_start_mosdns() {
    local config_dir="$1"
    local config_file="$2"

    echo -e "\033[31m$(date): 开始安装并启动 mosdns 服务\033[0m"
    if mosdns service install -d "$config_dir" -c "$config_file" && mosdns service start; then
        echo -e "\033[31m$(date): 安装并启动 mosdns 服务完成\033[0m"
    else
        echo -e "\033[31m$(date): 安装并启动 mosdns 服务失败\033[0m"
        exit 1
    fi
}

# 更新软件库
echo -e "\033[31m$(date): 软件库升级完成\033[0m"

# 安装所需软件
echo -e "\033[31m$(date): 开始安装软件\033[0m"
if apt install unzip wget curl redis-server vim docker-compose docker.io -y; then
    echo -e "\033[31m$(date): 软件安装完成\033[0m"
else
    echo -e "\033[31m$(date): 软件安装失败\033[0m"
    exit 1
fi

# 下载并解压 mosdns
download_and_unzip "https://mirror.ghproxy.com/https://github.com/IrineSistiana/mosdns/releases/download/v4.5.3/mosdns-linux-amd64.zip" "/usr/local/bin"
chmod +x /usr/local/bin/mosdns

# 下载并解压 easymosdns
download_and_unzip "https://mirror.ghproxy.com/https://github.com/haotianlPM/easymosdns-k/releases/download/v1.0.0/easymosdns-k.zip" "/etc/mosdns"
mv "/etc/mosdns/easymosdns-k-main" "/etc/mosdns"

# 创建 /etc/systemd/resolved.conf.d 目录
create_directory "/etc/systemd/resolved.conf.d"

# 创建 /etc/systemd/resolved.conf.d/dns.conf 文件
create_file "/etc/systemd/resolved.conf.d/dns.conf" "[Resolve]\nDNS=127.0.0.1\nDNSStubListener=no"

# 备份并创建符号链接 /etc/resolv.conf
echo -e "\033[31m$(date): 开始备份并创建符号链接 /etc/resolv.conf\033[0m"
if mv /etc/resolv.conf /etc/resolv.conf.backup && ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf; then
    echo -e "\033[31m$(date): 备份并创建符号链接 /etc/resolv.conf 完成\033[0m"
else
    echo -e "\033[31m$(date): 备份并创建符号链接 /etc/resolv.conf 失败\033[0m"
    exit 1
fi

# 重新加载或重启 systemd-resolved
echo -e "\033[31m$(date): 开始重新加载或重启 systemd-resolved\033[0m"
if systemctl reload-or-restart systemd-resolved; then
    echo -e "\033[31m$(date): 重新加载或重启 systemd-resolved 完成\033[0m"
else
    echo -e "\033[31m$(date): 重新加载或重启 systemd-resolved 失败\033[0m"
    exit 1
fi

# 下载 mosdns 配置文件
download_and_unzip "https://raw.gitcode.com/shangkouyou/GM/raw/main/mosdns.yaml" "/etc/mosdns"

# 安装并启动 mosdns 服务
install_and_start_mosdns "/etc/mosdns" "/etc/mosdns/mosdns.yaml"

# 创建目录 /scu/docker
echo -e "\033[31m$(date): 开始创建目录 /scu/docker\033[0m"
if mkdir -p /scu/docker; then
    echo -e "\033[31m$(date): 创建目录 /scu/docker 完成\033[0m"
else
    echo -e "\033[31m$(date): 创建目录 /scu/docker
    
# 进入路径 /scu/docker
cd /scu/docker || exit 1

# 下载文件 docker.zip
echo -e "\033[31m$(date): 开始下载文件 docker.zip\033[0m"
if wget --no-check-certificate -q -O docker.zip https://gitcode.net/qq_43662828/metaclash/-/raw/master/mosdnsdocker/docker.zip; then
    echo -e "\033[31m$(date): 下载文件 docker.zip 完成\033[0m"
else
    echo -e "\033[31m$(date): 下载文件 docker.zip 失败\033[0m"
    exit 1
fi

# 解压文件到根路径
echo -e "\033[31m$(date): 开始解压文件到根路径\033[0m"
if unzip -o docker.zip -d /; then
    echo -e "\033[31m$(date): 解压文件完成\033[0m"
else
    echo -e "\033[31m$(date): 解压文件失败\033[0m"
    exit 1
fi

# 检查 Docker Compose 是否安装
echo -e "\033[31m$(date): 检查 Docker Compose 是否安装\033[0m"
if command -v docker-compose >/dev/null 2>&1; then
    echo -e "\033[31m$(date): Docker Compose 已安装\033[0m"
else
    echo -e "\033[31m$(date): Docker Compose 未安装，将尝试安装\033[0m"
    if curl -fsSL https://get.docker.com -o get-docker.sh; then
        sh get-docker.sh
        rm -f get-docker.sh
    else
        echo -e "\033[31m$(date): 安装 Docker Compose 失败\033[0m"
        exit 1
    fi
fi

# 强制更新 Docker Compose
echo -e "\033[31m$(date): 开始强制更新 Docker Compose\033[0m"
if curl -fsSL https://mirror.ghproxy.com/https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose; then
    chmod +x /usr/local/bin/docker-compose
    echo -e "\033[31m$(date): 强制更新 Docker Compose 完成\033[0m"
else
    echo -e "\033[31m$(date): 强制更新 Docker Compose 失败\033[0m"
    exit 1
fi

# 执行 docker-compose up -d
echo -e "\033[31m$(date): 开始执行 docker-compose up -d\033[0m"
if docker-compose up -d; then
    echo -e "\033[31m$(date): 执行 docker-compose up -d 完成\033[0m"
else
    echo -e "\033[31m$(date): 执行 docker-compose up -d 失败\033[0m"
    exit 1
fi
