#!/bin/bash
# 软件库升级
apt update
echo "软件库升级完成"

# 安装所需软件
apt install unzip wget curl vim net-tools bird2 -y
echo "软件安装完成"

wget https://gitcode.net/qq_43662828/metaclash/-/raw/master/meta.zip
echo "需要手动把文件放进 /scu 目录"
echo "准备配置文件要先完成"

mkdir /scu
echo "创建文件ok"

gzip -d meta.zip.gz
echo "解压所需要文件ok"

unzip meta.zip
echo "准备配置文件完成"

mv meta /scu/meta
echo "准备配置文件已经归位"

# 解压 开始配置
cd /scu/meta
echo "解压并设置 Scu配置文件 完成"

# 开始添加执行权限
chmod +x metaclash
echo "开始添加执行权限完成" 

# 开始复制 clash 到 /usr/local/bin
cp metaclash /usr/local/bin
echo "复制 clash 到 /usr/local/bin完成" 

# 开始创建 systemd 服务
tee /etc/systemd/system/metaclash.service > /dev/null <<EOF
[Unit]
Description=Clash daemon, A rule-based proxyin Go.
After=network.target
[Service]
Type=simple
Restart=always
ExecStart=/usr/local/bin/metaclash -d /scu/meta
[Install]
WantedBy=multi-user.target
EOF
echo "创建 systemd 服务完成" 

# 开机启动
systemctl enable metaclash
echo "开机启动配置完成" 

# 开始启动
systemctl start metaclash
echo "开始启动完成" 
 
# 状态查看
systemctl status metaclash
echo "服务都弄完啦开始愉快的玩吧吧"