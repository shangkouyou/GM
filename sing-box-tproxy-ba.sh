#!/bin/bash

# Create the service files
sudo touch /etc/systemd/system/singbox-route.service
sudo nano /etc/systemd/system/singbox-route.service << EOF
[Unit]
Description=sing-box TProxy Rules
After=network.target
Wants=network.target

[Service]
User=root
Type=oneshot
RemainAfterExit=yes
# there must be spaces before and after semicolons
ExecStart=/sbin/ip rule add fwmark 1 table 100 ; /sbin/ip route add local default dev lo table 100 ; /sbin/ip -6 rule add fwmark 1 table 101 ; /sbin/ip -6 route add local ::/0 dev lo table 101
ExecStop=/sbin/ip rule del fwmark 1 table 100 ; /sbin/ip route del local default dev lo table 100 ; /sbin/ip -6 rule del fwmark 1 table 101 ; /sbin/ip -6 route del local ::/0 dev lo table 101

[Install]
WantedBy=multi-user.target
EOF

sudo touch /etc/systemd/system/nftables1.service
sudo nano /etc/systemd/system/nftables1.service << EOF
[Unit]
Description=NFTables Firewall
After=network.target

[Service]
ExecStart=/usr/sbin/nft -f /etc/nftables.conf
Type=oneshot
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
EOF

# Configure nftables
sudo nano /etc/nftables.conf << EOF
#!/usr/sbin/nft -f

flush ruleset

define RESERVED_IP = {
    100.64.0.0/10,
    127.0.0.0/8,
    169.254.0.0/16,
    172.16.0.0/12,
    192.0.0.0/24,
    224.0.0.0/4,
    240.0.0.0/4,
    255.255.255.255/32
}

table ip sing-box {
    chain prerouting {
        type filter hook prerouting priority mangle; policy accept;
        ip daddr $RESERVED_IP return
        # Replace with your internal network subnet
        ip daddr 10.0.0.0/24 tcp dport != 53 return
        ip daddr 10.0.0.0/24 udp dport != 53 return
        # Replace with your transparent proxy port
        ip protocol tcp tproxy to :3344 meta mark set 1
        ip protocol udp tproxy to :3344 meta mark set 1
    }
    chain output {
        type route hook output priority mangle; policy accept;
        ip daddr $RESERVED_IP return
        # Replace with your internal network subnet
        ip daddr 10.0.0.0/24 tcp dport != 53 return
        ip daddr 10.0.0.0/24 udp dport != 53 return
        meta mark set 1234 return
        ip protocol tcp meta mark set 1
        ip protocol udp meta mark set 1
    }
}
EOF

# Enable and start the services
sudo nft flush ruleset && nft -f /etc/nftables.conf && nft list ruleset && \
sudo systemctl enable --now nftables1 && sudo systemctl restart nftables1 && \
sudo systemctl enable --now singbox-route && sudo systemctl restart singbox-route
