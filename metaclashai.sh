#!/bin/bash
set -e

LOG_FILE="/var/log/metaclash_install.log"

print_msg() {
    RED='\033[0;31m'  # ANSI escape code for red color
    NC='\033[0m'      # ANSI escape code to reset color
    echo -e "[ $(date +'%Y-%m-%d %H:%M:%S') ] ${RED}$1${NC}" | tee -a "$LOG_FILE"
}

# Software library update and installation
update_and_install() {
    print_msg "开始软件库升级"
    
    # Update the software library, but don't exit on error
    if apt update; then
        print_msg "软件库升级完成"
    else
        print_msg "软件库升级失败，继续执行下面的步骤"
    fi

    # Install necessary software
    if apt install -y unzip wget curl vim net-tools bird2; then
        print_msg "软件安装完成"
    else
        print_msg "软件安装失败"
        exit 1
    fi
}

# Download and prepare configuration files
download_and_prepare_config() {
    print_msg "开始下载和准备配置文件"
    mkdir -p /scu/meta
    cd /scu/meta
    wget --no-check-certificate -O meta.zip https://gitcode.net/qq_43662828/metaclash/-/raw/master/meta.zip
    unzip -o meta.zip
    print_msg "配置文件准备完成"
}

# Install metaclash
install_metaclash() {
    print_msg "开始安装 metaclash"
    cd /scu/meta
    chmod +x metaclash
    cp metaclash /usr/local/bin
    print_msg "metaclash 安装完成"
}

# Configure systemd service
configure_systemd_service() {
    print_msg "开始配置 systemd 服务"
    cat <<EOF > /etc/systemd/system/metaclash.service
[Unit]
Description=metaclash 守护进程，基于规则的 Go 代理
After=network.target

[Service]
Type=simple
Restart=always
ExecStart=/usr/local/bin/metaclash -d /scu/meta

[Install]
WantedBy=multi-user.target
EOF
    print_msg "systemd 服务配置完成"
}

# Enable and start the service
enable_and_start_service() {
    print_msg "启用并启动 metaclash 服务"
    systemctl enable metaclash
    systemctl start metaclash
    print_msg "开机启动配置完成"
    print_msg "后台访问地址：IP:9090/ui/"
    print_msg "后台访问密码：1381ni"
    print_msg "机场订阅修改路径如下"
    print_msg "/scu/meta/config.yaml（24行）"
    print_msg "订阅文件粘贴第（24行）https后即可"
    print_msg "脚本执行完成✨，愉快的玩玩耍吧吧吧✨"
    print_msg "脚本运行状态参考下面情况自行判断把！✨"
    systemctl status metaclash
}

# Main function
main() {
    print_msg "✨全自动脚本执行开始安装啦✨"
    update_and_install
    download_and_prepare_config
    install_metaclash
    configure_systemd_service
    enable_and_start_service
    print_msg "脚本执行完成，愉快的玩吧吧"
}

# Execute main function
main