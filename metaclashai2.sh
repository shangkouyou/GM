#!/bin/bash
set -e

print_msg() {
    echo "[ $(date +'%Y-%m-%d %H:%M:%S') ] $1"
}

# 更新软件库和安装软件
update_and_install() {
    print_msg "开始软件库升级和安装所需软件"
    apt update
    apt install -y unzip wget curl vim net-tools bird2
    print_msg "软件库升级和软件安装完成"
}

# 下载和准备配置文件
download_and_prepare_config() {
    print_msg "开始下载和准备配置文件"
    mkdir -p /scu
    cd /scu
    wget -O meta.zip https://gitcode.net/qq_43662828/metaclash/-/raw/master/meta.zip
    unzip -o meta.zip
    mv -f meta /scu/meta
    print_msg "配置文件准备完成"
}

# 安装 metaclash
install_metaclash() {
    print_msg "开始安装 metaclash"
    cd /scu/meta
    chmod +x metaclash
    cp metaclash /usr/local/bin
    print_msg "metaclash 安装完成"
}

# 配置 systemd 服务
configure_systemd_service() {
    print_msg "开始配置 systemd 服务"
    cat <<EOF > /etc/systemd/system/metaclash.service
[Unit]
Description=metaclash 守护进程，基于规则的 Go 代理
After=network.target

[Service]
Type=simple
Restart=always
ExecStart=/usr/local/bin/metaclash -d /scu/meta

[Install]
WantedBy=multi-user.target
EOF
    print_msg "systemd 服务配置完成"
}

# 启用并启动服务
enable_and_start_service() {
    print_msg "启用并启动 metaclash 服务"
    systemctl enable metaclash
    systemctl start metaclash
    print_msg "开机启动配置完成"
    systemctl status metaclash
    print_msg "后台访问地址：IP:9090/ui/"
    print_msg "后台访问密码：1381ni"
}

# 主函数
main() {
    update_and_install
    download_and_prepare_config
    install_metaclash
    configure_systemd_service
    enable_and_start_service
    print_msg "脚本执行完成，愉快的玩吧吧"
}

# 执行主函数
main
