#!/bin/bash

# 软件库升级
echo -e "\033[31m$(date): 软件库升级完成\033[0m"

# 安装所需软件
echo -e "\033[31m$(date): 开始安装软件\033[0m"
apt install unzip wget curl redis-server vim docker-compose docker.io -y
echo -e "\033[31m$(date): 软件安装完成\033[0m"

# 下载 mosdns
echo -e "\033[31m$(date): 开始下载 mosdns\033[0m"
wget https://mirror.ghproxy.com/https://github.com/IrineSistiana/mosdns/releases/download/v4.5.3/mosdns-linux-amd64.zip
echo -e "\033[31m$(date): 下载 mosdns 完成\033[0m"

# 下载 easymosdns
echo -e "\033[31m$(date): 开始下载 easymosdns\033[0m"
wget https://mirror.ghproxy.com/https://github.com/haotianlPM/easymosdns-k/releases/download/v1.0.0/easymosdns-k.zip
echo -e "\033[31m$(date): 下载 easymosdns 完成\033[0m"

# 解压 mosdns
echo -e "\033[31m$(date): 开始解压 mosdns\033[0m"
unzip mosdns-linux-amd64.zip "mosdns" -d /usr/local/bin
chmod +x /usr/local/bin/mosdns
echo -e "\033[31m$(date): 解压并设置 mosdns 完成\033[0m"

# 解压 easymosdns
echo -e "\033[31m$(date): 开始解压 easymosdns\033[0m"
unzip easymosdns-k.zip
mv easymosdns-k-main /etc/mosdns
echo -e "\033[31m$(date): 解压并设置 easymosdns 完成\033[0m"

# 创建 /etc/systemd/resolved.conf.d 目录
echo -e "\033[31m$(date): 开始创建 /etc/systemd/resolved.conf.d 目录\033[0m"
mkdir -p /etc/systemd/resolved.conf.d
echo -e "\033[31m$(date): 创建 /etc/systemd/resolved.conf.d 目录完成\033[0m"

# 创建 /etc/systemd/resolved.conf.d/dns.conf 文件
echo -e "\033[31m$(date): 开始创建 /etc/systemd/resolved.conf.d/dns.conf 文件\033[0m"
tee /etc/systemd/resolved.conf.d/dns.conf <<EOF
[Resolve]
DNS=127.0.0.1
DNSStubListener=no
EOF
echo -e "\033[31m$(date): 创建 /etc/systemd/resolved.conf.d/dns.conf 文件完成\033[0m"

# 备份并创建符号链接 /etc/resolv.conf
echo -e "\033[31m$(date): 开始备份并创建符号链接 /etc/resolv.conf\033[0m"
mv /etc/resolv.conf /etc/resolv.conf.backup
ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf
echo -e "\033[31m$(date): 备份并创建符号链接 /etc/resolv.conf 完成\033[0m"

# 重新加载或重启 systemd-resolved
echo -e "\033[31m$(date): 开始重新加载或重启 systemd-resolved\033[0m"
systemctl reload-or-restart systemd-resolved
echo -e "\033[31m$(date): 重新加载或重启 systemd-resolved 完成\033[0m"

# 下载 mosdns 配置文件
echo -e "\033[31m$(date): 开始下载 mosdns 配置文件\033[0m"
cd /etc/mosdns
rm -f config.yaml
wget -O mosdns.yaml https://raw.gitcode.com/shangkouyou/GM/raw/main/mosdns.yaml
echo -e "\033[31m$(date): 下载 mosdns 配置文件完成\033[0m"

# 安装并启动 mosdns 服务
echo -e "\033[31m$(date): 开始安装并启动 mosdns 服务\033[0m"
mosdns service install -d /etc/mosdns -c mosdns.yaml
mosdns service start
echo -e "\033[31m$(date): 安装并启动 mosdns 服务完成\033[0m"

# 创建目录 /scu/docker
echo -e "\033[31m$(date): 开始创建目录 /scu/docker\033[0m"
if mkdir -p /scu/docker; then
    echo -e "\033[31m$(date): 创建目录 /scu/docker 完成\033[0m"
else
    echo -e "\033[31m$(date): 创建目录 /scu/docker 失败\033[0m"
    exit 1
fi

# 进入路径 /scu/docker
cd /scu/docker || exit 1

# 下载文件 docker.zip
echo -e "\033[31m$(date): 开始下载文件 docker.zip\033[0m"
if wget --no-check-certificate -q -O docker.zip https://gitcode.net/qq_43662828/metaclash/-/raw/master/mosdnsdocker/docker.zip; then
    echo -e "\033[31m$(date): 下载文件 docker.zip 完成\033[0m"
else
    echo -e "\033[31m$(date): 下载文件 docker.zip 失败\033[0m"
    exit 1
fi

# 解压文件到根路径
echo -e "\033[31m$(date): 开始解压文件到根路径\033[0m"
if unzip -o docker.zip -d /; then
    echo -e "\033[31m$(date): 解压文件完成\033[0m"
else
    echo -e "\033[31m$(date): 解压文件失败\033[0m"
    exit 1
fi

# 检查 Docker Compose 是否安装
echo -e "\033[31m$(date): 检查 Docker Compose 是否安装\033[0m"
if command -v docker-compose >/dev/null 2>&1; then
    echo -e "\033[31m$(date): Docker Compose 已安装\033[0m"
else
    echo -e "\033[31m$(date): Docker Compose 未安装，将尝试安装\033[0m"
    if curl -fsSL https://get.docker.com -o get-docker.sh; then
        sh get-docker.sh
        rm -f get-docker.sh
    else
        echo -e "\033[31m$(date): 安装 Docker Compose 失败\033[0m"
        exit 1
    fi
fi

# 强制更新 Docker Compose
echo -e "\033[31m$(date): 开始强制更新 Docker Compose\033[0m"
if curl -fsSL https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose; then
    chmod +x /usr/local/bin/docker-compose
    echo -e "\033[31m$(date): 强制更新 Docker Compose 完成\033[0m"
else
    echo -e "\033[31m$(date): 强制更新 Docker Compose 失败\033[0m"
    exit 1
fi

# 执行 docker-compose up -d
echo -e "\033[31m$(date): 开始执行 docker-compose up -d\033[0m"
if docker-compose up -d; then
    echo -e "\033[31m$(date): 执行 docker-compose up -d 完成\033[0m"
else
    echo -e "\033[31m$(date): 执行 docker-compose up -d 失败\033[0m"
    exit 1
fi

# 下载 docker 镜像
# echo -e "\033[31m$(date): 开始下载 docker 镜像\033[0m"
# mkdir -p /scu/docker
# cd /scu/docker
# wget --no-check-certificate -q -O docker.zip https://gitcode.net/qq_43662828/metaclash/-/raw/master/mosdnsdocker/docker.zip
# unzip -o docker.zip
# apt install docker-compose
# docker-compose up -d
# echo -e "\033[31m$(date): 下载 docker 镜像完成\033[0m"

# 安装 AdGuardHome
echo -e "\033[31m$(date): 开始安装 AdGuardHome\033[0m"
wget -O https://raw.gitcode.com/shangkouyou/GM/raw/main/adguardhome.sh && bash adguardhome.sh
#wget --no-verbose -O - https://gcore.jsdelivr.net/gh/AdguardTeam/AdGuardHome@master/scripts/install.sh | sh -s -- -v
cd /opt/AdGuardHome
rm -f AdGuardHome.yaml
wget -O AdGuardHome.yaml https://raw.gitcode.com/shangkouyou/GM/raw/main/AdGuardHome.yaml
echo -e "\033[31m$(date): 安装 AdGuardHome 完成\033[0m"