#!/bin/bash

# 脚本版本
VERSION="1.0.0"

# 检查系统要求
if [ "$(uname -m)" != "x86_64" ]; then
  echo "Sing-Box 仅支持 64 位系统。"
  exit 1
fi

if [ "$(lsb_release -r | awk '{print $2}')" -lt "18.04" ]; then
  echo "Sing-Box 要求 Ubuntu 18.04 LTS 或更高版本。"
  exit 1
fi

# 升级软件库
apt update

# 安装所需软件
apt install sudo unzip wget curl vim net-tools

# 下载 Sing-Box 安装包
# 指定下载位置
install_dir="/opt/sing-box"
arch=$(uname -m)
filename="sing-box_${VERSION}_${arch}.deb"
url="https://github.com/SagerNet/sing-box/releases/download/v${VERSION}/${filename}"
wget $url -P $install_dir

# 检查安装包是否存在
if [ ! -f $install_dir/$filename ]; then
  echo "Sing-Box 安装包不存在。"
  exit 1
fi

# 安装 Sing-Box
dpkg -i $install_dir/$filename

# 启动 Sing-Box 服务
systemctl start sing-box

# 提示用户
echo "Sing-Box 安装成功。"
echo "请使用以下命令启动 Sing-Box："
echo "systemctl start sing-box"
