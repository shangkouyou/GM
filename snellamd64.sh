#!/bin/bash

# 下载 Snell 服务器
wget https://dl.nssurge.com/snell/snell-server-v4.0.1-linux-amd64.zip

# 解压 Snell 服务器
unzip snell-server-v4.0.1-linux-amd64.zip -d /usr/local/bin

# 赋予 Snell 服务器二进制文件执行权限
chmod +x /usr/local/bin/snell-server

# 创建配置文件夹
mkdir -p /etc/snell

# 将 Snell 服务器配置写入文件
echo "
[snell-server]
listen = 0.0.0.0:11807
psk = 1381niXYMsnellamd64
ipv6 = false
" | tee /etc/snell/snell-server.conf

# 创建 Systemd 服务文件
echo "
[Unit]
Description=Snell 代理服务
After=network.target

[Service]
Type=simple
User=nobody
Group=nogroup
LimitNOFILE=32768
ExecStart=/usr/local/bin/snell-server -c /etc/snell/snell-server.conf
AmbientCapabilities=CAP_NET_BIND_SERVICE
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=snell-server

[Install]
WantedBy=multi-user.target
" | tee /etc/systemd/system/snell.service

# 重新加载 Systemd 以应用更改
systemctl daemon-reload

# 启用 Snell 服务器开机自启
systemctl enable snell

# 启动 Snell 服务器
systemctl start snell

# 检查 Snell 服务器状态
systemctl status snell

# 显示 Snell 服务器配置
cat /etc/snell/snell-server.conf

# 输出 Surge 配置
echo "
$(hostname) = snell, $(hostname -I | awk '{print $1}'), 11807, psk=1381niXYMsnellamd64, version=4, tfo=true
"